<?php
/**
 * @file
 * Auro's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * themes/auro directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. themes/auro.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['page_top']: Items for the page top region.
 * - $page['header']: Items for the header region.
 * - $page['social_links']: Items for the social links region.
 * - $page['search']: Items for the search region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the first second.
 * - $page['footer_first']: Items for the first footer column.
 * - $page['footer_second']: Items for the second footer column.
 * - $page['footer_third']: Items for the third footer column.
 * - $page['footer']: Items for the footer region.
 * - $page['page_bottom']: Items for the page bottom region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<div id="header-wrap">
    <!-- ______________________ HEADER _______________________ -->
    <header id="header">
    <div class="container"> 
	  <!-- BEGIN logo -->
      <?php if ($logo): ?>
	  	<div id="logo">
        <a href="<?php print $front_page; ?>" 
		title="<?php print $site_name; ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"/>
        </a>
		</div>
      <?php endif; ?>
	  <!-- End logo -->
	  <!-- Start header social link block-->
	  <?php if ($page['social_links']): ?>		   
		<div id="social-links-block">
			<?php print render($page['social_links']); ?>
		</div>
	  <?php endif; ?>
	  <!-- End header social link block-->	  
	  <br class="clear">
      <?php if (!$logo): ?>
	  <?php if ($site_name || $site_slogan): ?>
        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home"><?php print $site_name; ?></a>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home"><?php print $site_name; ?></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>
		<?php endif; ?>
        <?php if ($site_slogan): ?>
          <div id="site-slogan"><?php print $site_slogan; ?></div>
        <?php endif; ?>
      <?php endif; ?>

      <?php if ($page['header']): ?>
        <div id="header-region">
          <?php print render($page['header']); ?>
        </div>
      <?php endif; ?>
    </div>

	<!-- Start header search block-->
	<?php if ($page['search']): ?>
		<div id="header-search-block">	
		<?php print render($page['search']); ?>
		</div>
	<?php endif; ?>
	<!-- End header search block-->
	
	<br class="clear">
	
  <!-- ______________________ NAVIGATION _______________________ -->
  
  <?php if ($main_menu || $secondary_menu): ?>
    <nav id="navigation" class="menu <?php if (!empty($main_menu)) {print "with-primary";}
      if (!empty($secondary_menu)) {print " with-secondary";} ?>">
      <div class="container">
        <?php
          print theme('links', array(
            'links' => $main_menu,
            'attributes' => array(
              'id' => 'primary',
              'class' => array('main-menu'),
            ),
          )
          );
          print theme('links', array(
            'links' => $secondary_menu,
            'attributes' => array(
              'id' => 'secondary',
              'class' => array('sub-menu'),
            ),
          )
          );
        ?>
      </div>
    </nav> <!-- /navigation -->
  <?php endif; ?>
	
  </header> <!-- /header -->
  
</div>
<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ MAIN _______________________ -->

  <div id="main">
    <div class="container">
      <section id="content">
  
          <?php if ($breadcrumb || $title|| $messages || $tabs || $action_links): ?>
            <!-- <div id="content-header"> -->

              <?php print $breadcrumb; ?>

              <?php if ($page['highlighted']): ?>
                <div id="highlighted"><?php print render($page['highlighted']) ?></div>
              <?php endif; ?>

              <?php print render($title_prefix); ?>

              <?php if ($title): ?>
                <h1 class="title"><?php print $title; ?></h1>
              <?php endif; ?>

              <?php print render($title_suffix); ?>
              <?php print $messages; ?>
              <?php print render($page['help']); ?>

              <?php if (render($tabs)): ?>
                <div class="tabs"><?php print render($tabs); ?></div>
              <?php endif; ?>

              <?php if ($action_links): ?>
                <ul class="action-links"><?php print render($action_links); ?></ul>
              <?php endif; ?>

            <!-- </div> /#content-header -->
          <?php endif; ?>

          <div id="content-area">
            <?php print render($page['content']) ?>
          </div>

          <?php print $feed_icons; ?>

      </section> <!-- /content-inner /content -->

      <?php if ($page['sidebar_first']): ?>
        <aside id="sidebar-first">
          <?php print render($page['sidebar_first']); ?>
        </aside>
      <?php endif; ?> <!-- /sidebar-first -->

      <?php if ($page['sidebar_second']): ?>
        <aside id="sidebar-second">
          <?php print render($page['sidebar_second']); ?>
        </aside>
      <?php endif; ?> <!-- /sidebar-second -->
    </div>
  </div> <!-- /main -->

  <!-- ______________________ FOOTER _______________________ -->

  <footer>
	<div id="footer-wrap">
	  <div id="big-footer">
		  <div class="footer-content col-option3">
			  <?php if ($page['footer_first']): ?>
				<div class="footer-column">
				  <?php print render($page['footer_first']); ?>
				</div> <!-- /footer first-->
			  <?php endif; ?>  
			  <?php if ($page['footer_second']): ?>
				<div class="footer-column">
				  <?php print render($page['footer_second']); ?>
				</div> <!-- /footer second-->
			  <?php endif; ?>  
			  <?php if ($page['footer_third']): ?>
				<div class="footer-column">
				  <?php print render($page['footer_third']); ?>
				</div> <!-- /footer third-->
			  <?php endif; ?>
			  <br class="clear">	
		  </div>
	  </div>
	  <div id="standard-footer">
	  <?php if ($page['footer']): ?>
		<div id="footer">
		  <?php print render($page['footer']); ?>
		</div> <!-- /footer -->
	  <?php endif; ?>
	  </div>
	</div>  
  </footer> <!-- /footer -->

</div> <!-- /page -->
